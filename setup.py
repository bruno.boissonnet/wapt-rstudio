# -*- coding: utf-8 -*-
from setuphelpers import *

r"""
Usable WAPT package functions: install(), uninstall(), session_setup(), audit(), update_package()

"""

# Declaring global variables - Warnings:
# 1) WAPT context is only available in package functions;
# 2) Global variables are not persistent between calls


def install():
    print("--> DÉBUT DE L'INSTALLATION")

    # Récupère le nom du logiciel
    SoftwareName = control.get('name')
    print(f"Nom du logiciel     : {SoftwareName}.")

    # Récupère le numéro de version du logiciel (sans l'ajout de WAPT)
    RStudioversion = control.get_software_version().split('-',1)[0]
    print(f"Version du logiciel : {RStudioversion}.")

    # Récupère le nom de l'exécutable d'installation
    bin_name = glob.glob(f"{SoftwareName}-*.exe")[0]

    # Installe le logiciel avec les paramètres
    print(f"Nom de l'exécutable : {bin_name}")

    install_exe_if_needed(
        bin_name,
        silentflags='/S',
        key=SoftwareName,
        min_version=RStudioversion
    )
    # La clé (key) est récupérée ici : Ordinateur\HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\RStudio

    print("--> FIN DE L'INSTALLATION.")



